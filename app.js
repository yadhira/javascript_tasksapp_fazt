// capturar el evento submit del formulario
document.getElementById('formTask').addEventListener('submit', saveTask);

// guardar tareas
function saveTask(e) {
  let title = document.getElementById('txttitle').value;
  let description = document.getElementById('txtdescription').value;
  // console.log(description)

  //almacenando en un obj las tareas
  let task = {
    title,//title:title
    description//description:description
  };

  if(localStorage.getItem('tasks') === null) {//si el localstorage esta vacio
    //crea la tarea
    let tasks = [];
    tasks.push(task);
    localStorage.setItem('tasks', JSON.stringify(tasks));
  } else {
    // actualiza la tarea
    let tasks = JSON.parse(localStorage.getItem('tasks'));
    tasks.push(task);
    localStorage.setItem('tasks', JSON.stringify(tasks));
  }

  // lista tareas
  getTasks();
  document.getElementById('formTask').reset();//limpia los inputs del form
  e.preventDefault();//previene la accion por defecto del evento(submit)
}

// eliminar tarea por su titulo
function deleteTask(title) {
  // console.log(title)
  let tasks = JSON.parse(localStorage.getItem('tasks'));
  for(let i = 0; i < tasks.length; i++) {
    if(tasks[i].title == title) {
      tasks.splice(i, 1);
    }
  }
  
  localStorage.setItem('tasks', JSON.stringify(tasks));
  getTasks();
}

//obtener las tareas
function getTasks() {
  let tasks = JSON.parse(localStorage.getItem('tasks'));
  let tasksView = document.getElementById('tasks');
  tasksView.innerHTML = '';
  for(let i = 0; i < tasks.length; i++) {
    let title = tasks[i].title;
    let description = tasks[i].description;

    tasksView.innerHTML += `<div class="card mb-3">
        <div class="card-body">
          <p>${title} - ${description}
          <a href="#" onclick="deleteTask('${title}')" class="btn btn-danger ml-5">Delete</a>
          </p>
        </div>
      </div>`;
  }
}

getTasks();